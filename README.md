# FIDO Alliance WebAuthn demo

### Requirements

- Firefox Nightly / Google Chrome
- Nodejs + NPM

### Run

- `git clone`
- `cd webauthn-demo`
- `npm install`
- `node app`
-  Launch localhost:3000
